//
//  SwiftUI1App.swift
//  SwiftUI1
//
//  Created by Balraj Verma on 9/21/20.
//

import SwiftUI

@main
struct SwiftUI1App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
