//
//  UserHome.swift
//  SwiftUI1
//
//  Created by Balraj verma on 9/22/20.
//

import SwiftUI

struct UserHome: View {
   
    
    //To fetch the predefined properties which are system defined and can be used. Semple 
    @Environment(\.colorScheme) var colorScheme
    // what properties can be picked up can be see here : //https://developer.apple.com/documentation/swiftui/environmentvalues
    
    
    /*
    If a view needs to share control of state with a child view, declare a property in the child with the Binding property wrapper. A binding represents a reference to existing storage, preserving a single source of truth for the underlying data.
 */
    @Binding var loggedInUser: String
    @AppStorage(GlobalAppStorageValue.emailFromUserDefaults) var valOnOtherPage = ""
    var action = ()
    
    var body: some View {
        VStack{
            Label(
                title: { Text("\(loggedInUser) From @State/@Binding")
                    .multilineTextAlignment(.leading)
                },
                icon: { Image(systemName: "") }
            )
            .foregroundColor(Color.red)
            .font(.callout)
            .frame(width: 350, height: 40, alignment: .center)
            .padding(40)
            
            Label(
                title: { Text("\(valOnOtherPage) from @AppStorage ")
                    .multilineTextAlignment(.leading)
                },
                icon: { Image(systemName: "") }
            )
            .foregroundColor(Color.red)
            .font(.callout)
            .frame(width: 350, height: 40, alignment: .center)
            .padding(40)
            
            Label(
                title: { Text(colorScheme == .dark ? "Dark Mode" : "Light Mode from @Environment")
                    .multilineTextAlignment(.leading)
                },
                icon: { Image(systemName: "") }
            )
            .foregroundColor(Color.red)
            .font(.callout)
            .frame(width: 350, height: 40, alignment: .center)
            .padding(30)
            
            NavigationLink(
                destination: UserInfo(),
                label: {
                    Text("Data Flow Wrapper")
                }
            )
            .background(Color.clear)
            .foregroundColor(Color.blue)
        }
        
    }
}

struct UserHome_Previews: PreviewProvider {
    static var previews: some View {
        UserHome(loggedInUser: .constant(""))
    }
}
