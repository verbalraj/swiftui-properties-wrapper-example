//
//  UserInfo.swift
//  SwiftUI1
//
//  Created by Balraj Verma on 9/22/20.
//

import SwiftUI


class UserInfoDataFlow: ObservableObject {
    //If this value is the publisher to view this will be used as with wrapper @pulished
    @Published var email: String = "From ObservableClass"
    func changeEmail(newEmail: String) -> String {
        self.email = newEmail
        return self.email
    }
}


struct UserInfo: View {
    
    //So, here is trick if your view wants to change the data from userinfoDataFlow in reable you can use the model without any @property wrapper, But if View has capability to change the email of userInfoDataFlow to track that change you have to user the @observedObject wrapper to reflect the same on class and view both. Both will have the same copy of value.
   //@ObservedObject var model =  UserInfoDataFlow()
  // @StateObject var model =  UserInfoDataFlow()
  var model =  UserInfoDataFlow()

    var body: some View {
        Label(
            title: { Text(model.changeEmail(newEmail: "test Email"))
                .multilineTextAlignment(.leading)
            },
            icon: { Image(systemName: "") }
        )
        .foregroundColor(Color.red)
        .font(.callout)
        .frame(width: 350, height: 40, alignment: .center)
        .padding(40)

    }
}

struct UserInfo_Previews: PreviewProvider {
    static var previews: some View {
        UserInfo()
    }
}
