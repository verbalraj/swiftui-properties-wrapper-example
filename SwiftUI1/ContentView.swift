//
//  ContentView.swift
//  SwiftUI1
//
//  Created by Balraj Verma on 9/21/20.
//

import SwiftUI



enum GlobalAppStorageValue {
    static let emailFromUserDefaults = "email"
}


struct ContentView: View {
    
//    A property wrapper type that can read and write a value managed by SwiftUI

    @State private var emailAddress = ""
    @State private var password = ""
    @State private var isValidEmail = false
    @State private var showEmailAlert = false
    @State private var userAuthenticated = false
    //Can be use within View to pass data or to store and can be passed within other views.
    
    @AppStorage(GlobalAppStorageValue.emailFromUserDefaults) var email: String = ""
        
    /*
     Below property wrapper will show you how to use @AppStorage. This actully work alike userdefaults.
     
    */
    
    var buttonAction: () = ()
    init() {
        UINavigationBar.appearance().backgroundColor = .orange
        UINavigationBar.appearance().tintColor = .red
    }
    
    var body: some View {
        NavigationView{
            VStack{
                //email text field
                TextField(" test@gmail.com", text: $emailAddress)
                    .cornerRadius(10)
                    .textFieldStyle(RoundedBorderTextFieldStyle())
                    .frame(width: 300, height: 40, alignment: .leading)
                    // .background(Color.white)
                    .padding(EdgeInsets(top: 150, leading: 30, bottom: 0, trailing: 30))
                    .padding(30)
                    SecureField(" ****", text: $password)
                    .cornerRadius(10)
                    
                    .textFieldStyle(RoundedBorderTextFieldStyle())
                    .frame(width: 300, height: 40, alignment: .center)
                    .alert(isPresented: $showEmailAlert, content: {
                        Alert(title: Text("Email!"), message: Text("Email can't be blank"), dismissButton: .default(Text("OK!")))
                    }
                    )
                Spacer()
                Spacer()
                
                NavigationLink(destination: UserHome(loggedInUser: $emailAddress),
                                       isActive: $userAuthenticated) {
                    Button("Login", action: {
                        self.email = $emailAddress.wrappedValue
                        if ($emailAddress.wrappedValue.isEmpty && $password.wrappedValue.isEmpty) == false {
                            $userAuthenticated.wrappedValue = true
                            print("$userAuthenticated",$userAuthenticated)
                        } else {
                            if $emailAddress.wrappedValue.isEmpty {
                                $showEmailAlert.wrappedValue = true
                                $userAuthenticated.wrappedValue = false
                                print("$userAuthenticated",$userAuthenticated)
                            } else {
                                $showEmailAlert.wrappedValue = false
                                $userAuthenticated.wrappedValue = true
                                print("$userAuthenticated",$userAuthenticated)
                            }
                        }
                    })
                    
                }
                    .frame(width: 250, height: 35, alignment: .center)
                    .background(Color.blue)
                    .foregroundColor(Color.black)
                    .cornerRadius(20) // with corner radius you need to provide the rounded rectangle corner radiustoo.
                    .overlay(
                        RoundedRectangle(cornerRadius: 20).stroke(Color(.black), lineWidth: 1.5)
                    )
            }
            .padding(EdgeInsets(top: 0, leading: 0, bottom: 450, trailing: 0))
            .background(Color.gray)
            .navigationBarTitle("Login")
            .navigationBarTitleDisplayMode(.inline)
        }
    }
}

#if DEBUG
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            ContentView()
        }
    }
}
#endif
